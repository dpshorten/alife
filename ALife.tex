\documentclass[letterpaper]{article} \usepackage{natbib,alifeconf} \usepackage{hyperref}

\title{Evolvability as Biased Robustness} \author{David Shorten$^{1}$ \and Geoff Nitschke$^{1}$ \\ \mbox{}\\ $^1$University of
  Cape Town \\ dshorten@cs.uct.ac.za}


\begin{document}
\maketitle

\begin{abstract}
Robustness and evolvability have traditionally been seen as conflicting properties of evolutionary systems, due to the
fact that selection requires heritable variation on which to operate. Various recent studies have demonstrated that
organisms evolving in environments fluctuating non-randomly are able to become better at adapting to these fluctuations,
that is increase their \textit{evolvability} \citep{crombach2008evolution, draghi2008evolution}, and it has been
suggested that this is due to the emergence of biases in the mutational neighborhoods of genotypes
\citep{hogeweg2012toward, watson2014evolution}. This paper examines a natural consequence of these observations, which
is that a large bias towards certain phenotypes in an area of genotype space will lead to an increase in the robustness
of said phenotypes. The evolution of logic networks, which bear similarity to models of gene regulatory networks, is
simulated in environments which fluctuate between targets. It was found that an increase in evolvability is concomitant
with the emergence of highly robust genotypes. Analysis of the genotype space showed that evolution finds regions
containing robust genotypes coding for one of the target phenotypes and that these regions are either overlapping or
situated in close proximity, 
% NOTE TO GEOFF: I don't like this next statement, but I was looking for a way of saying that it is really cool that the
% robust regions are not always overlapping, and I couldn't find a better way of saying it.
demonstrating that biases in genotype space can have an interesting topology of their own.
These results are cast in the light of recent work showing how robustness facilitates evolvability by increasing the
accessibility of phenotypes through neutral networks \citep{wagner2008robustness, CilibertiWagner2007}.
\end{abstract}

%\vspace{5mm}

\section{Introduction}

An open question in artificial and natural life is whether digital and natural organisms undergoing an evolutionary
process are able to become better at adapting to the selective pressures presented to them, that is to become more
\textit{evolvable} \citep{WagnerAltenberg1996}. However, this question is complicated by multiple definitions of
\textit{evolvability}. In both evolutionary biology \citep{pigliucci2008evolvability, pigliucci2010genotype,
  wagner2008robustness, parter2008facilitated} and EC \citep{taraporecomparing}, for example, evolvability can refer to
either populations or individuals \citep{wilder2015reconciling}.  Similarly, within \textit{Evolutionary Computation} (EC)
\citep{EibenSmith2003} numerous definitions and associated metrics have been proposed.  For example, those that focus
exclusively on solution fitness \citep{grefenstette1999evolvability, reisinger2007acquiring} or variability of offspring
\citep{lehman2013evolvability}. \cite{taraporecomparing} developed a metric which incorporated both the fitness and
diversity of offspring. \cite{reisinger2005towards} measured evolvability as the ability of genotypes with various
representations to detect invariant patterns as commonalities in a changing fitness function.

Two significant definitions from the biological literature \footnote{For a review of evolvability in biology, the reader
  is referred to \cite{pigliucci2008evolvability}.}  which are of great pertinence to this work are those of
\textit{phenotypic accessibility} and \textit{adaptability}. The former refers to the proportion of phenotypes that can
be accessed through evolution \citep{wagner2008robustness, CilibertiWagner2007, CilibertiWagnerB2007}. This proportion
is determined by the topology of the genotype space (which other genotypes a given genotype can directly mutate to) and
the G$\,\to\,$P mapping (which phenotype each genotype codes for). Adaptability refers to the ability of organism to
adapt to changes in the environment. It has the advantage of not imposing an experimenter-biased measure on the system,
but merely asks whether evolution ``Delivers the goods'' \citep{budd2006origin, pigliucci2008evolvability}. A prevailing
hypothesis is that if the environment sufficiently varies over time, then organisms evolve the ability to be able to
evolve suitable adaptations to such environmental changes faster \citep{WagnerAltenberg1996, wagner2008robustness,
  draghi2008evolution}. \cite{crombach2008evolution} as well as \cite{draghi2008evolution} have demonstrated that
computational models of \textit{Gene Regulatory Networks} (GRNs) exhibit such \textit{evolvability}.

The representation problem in EC addresses the issue of how to represent and adapt (mutate and recombine) genotypes in
order that a broad range of complex solutions can be represented by relatively simple genotype encodings
\citep{wagner1996perspective}.  The choice of representation and associated operators has a significant impact on the
evolution of viable solutions and representations which facilitate evolution have been termed \textit{evolvable}
\citep{wagner1996perspective, rothlauf2006representations, simoes2014self}.  Similarly, in nature, genetic information
defining the form and function of an organism is stored within its genotype, however the developmental process which
translates this information into phenotypes (the G$\,\to\,$P map) is not well understood \citep{pigliucci2010genotype}.
Yet, it has become clear that the G$\,\to\,$P map is neither one-to-one nor linear
\citep{Gjuvsland2013}, in many organisms, including the case of \textit{Ribonucleic acid} RNA folding
\citep{DavidDraper1992} it has been found that many genotypes can code for a single phenotype and that genetic change
resulting from mutation is not proportional to phenotypic change \citep{pigliucci2010genotype, wagner2008robustness,
  parter2008facilitated}.

These features of the G$\,\to\,$P map have two important consequences on evolutionary dynamics. The first is that they
allow for the emergence of robust genotypes. As many genotypes can code for one phenotype, it is possible that some
number of a genotype's mutational neighbors code for the same phenotype as it does, thus affording the genotype a degree
of mutational robustness \citep{wagner2008robustness}. The second consequence is that it becomes plausible that certain
phenotypes are found in greater abundance in certain regions of the genotype space. This, in turn, results in genotypic
mutations having non-random effects on phenotypes, opening up the possibility that the distribution of these effects is
in some way advantageous \citep{hogeweg2012toward, watson2014evolution, parter2008facilitated, meyers2005evolution,
  gerhart2007theory, pavlicev2010evolution}.   

It should be intuitively obvious that both robustness and mutational bias will have an effect on evolvability. However,
robustness influences evolvability in a rather non-intuitive manner. Recent works  have shown that a certain degree of
robustness has a large benefit on evolvability interpreted either as phenotypic accessibility \citep{wagner2008robustness,
  CilibertiWagner2007} or adaptation to a goal \citep{draghi2010mutational}. These studies are predicated on the
assumption that a certain portion of genotypes are non-viable and will never produce offspring. This constrains the
portion of the genotype space that is accessible from a given genotype. They argue that once robustness rises above a
certain threshold, genotypes of a given phenotype get connected in large neutral networks which can be traversed in
order to access a large variety of phenotypes and that this access to variation allows for faster adaptation towards
a stationary target. 

Mutational biases, on the other hand, can increase the likelihood of a given phenotype occurring in the neighborhood of
a given genotype, thus facilitating adaptation towards it. Various recent studies have demonstrated that
organisms evolving in environments fluctuating non-randomly are able to become better at adapting to these fluctuations,
that is increase their \textit{evolvability} \citep{crombach2008evolution, draghi2008evolution}, and it has been
suggested that this is due to the emergence of biases in the mutational neighborhoods of genotypes
\citep{hogeweg2012toward, watson2014evolution}. This work seeks to examine a natural consequence of these
biases, which is that increasing the bias towards a small number of phenotypes within a region of genotype space will
increase the robustness of the genotypes within that region.

Experiments were conducted using the simulated evolution of logic networks in a fluctuating environment. These logic
networks were comprised of nodes containing either NAND logic gates or threshold functions. The networks composed of
threshold functions bear strong resemblance to models of gene regulatory networks used to demonstrate the emergence of
evolvability in varying environments \citep{crombach2008evolution, draghi2008evolution} and the networks composed of
NAND logic gates are very similar to those used to demonstrate the emergence of modularity \citep{KashtanAlon2005} as
well as speedups \citep{KashtanNoorAlon2007} in fluctuating environments. It was found that an increase in evolvability
is concomitant with the emergence of highly robust genotypes. Analysis of the genotype space showed that evolution finds
regions containing robust genotypes coding for one of the target phenotypes and that these regions are either
overlapping or situated in close proximity.

\section{Methods}\label{sec:methods}

\subsection{Network Models}\label{sec:GRN}

Networks were formed of nodes which could hold an activation value of either zero or one. Each node had an activation
function which was either a threshold or a NAND function. The activations of certain other nodes were used as the inputs
to these functions, the specification of which nodes' activations to use thus implied the connectivity of the
network. Updates were done synchronously. The threshold function used is specified in equation \ref{grn-update}.
\begin{equation}\label{grn-update} s_i(t + 1) = \left\{ \begin{array}{lr} 1 :& \sum_j w_{ij}s_j(t) > \theta_i\\
    0 :& \sum_j w_{ij}s_j(t) \leq \theta_i \end{array} \right.
\end{equation}
Where, $s_i(t)$ is the activation of the $i$th node at simulation iteration $t$, $w_{ij}$ is the connection
weight of the directed edge from the $i$th to the $j$th node, and $\theta_i$ is the threshold of the $i$th node.
If no such connection exists then $w_{ij} = 0$. Table \ref{NAND-parameters} presents the parameters of the NAND networks
and table \ref{threshold-parameters} presents the parameters of the threshold networks.

\begin{table}[t]
  \centering
  \begin{tabular}{lr} \hline
    \textbf{Parameter}                           &              \textbf{Value Range} \\
    \hline
    Weights ($w_{ij}$)                            &              $[-2, 2]$ \\
    Thresholds ($\theta_{ij}$)                    &              $[-3, 3]$ \\
    Number of nodes                              &              $20$    \\
    Incoming connections per node                &              $[0, 10]$ \\
    Simulation iterations (maximum $t$)          &              $6$ \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Parameters for the networks composed of nodes with threshold functions}\label{threshold-parameters}
\end{table}

\begin{table}[t]
  \centering
  \begin{tabular}{lr} \hline
    \textbf{Parameter}                           &              \textbf{Value Range} \\
    \hline
    Number of nodes                              &              $20$    \\
    Incoming connections per node                &              $2$ \\
    Simulation iterations (maximum $t$)          &              $6$ \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Parameters for the networks composed of nodes with threshold functions}\label{NAND-parameters}
\end{table}

\subsubsection{Mutation Operators}

Table \ref{mutations} specifies the mutation operators used in the evolutionary algorithm applied to evolve the networks.
The \texttt{mutate\_weight} operators were applied to the GRN node connections, where for every connection, the operator
was applied with a given probability (table \ref{evo-params}). All other mutation operators acted on nodes' activation
and threshold functions with a given probability (table \ref{evo-params}). In the evolution of networks with NAND
functions only the \texttt{mutate\_connection} operator was used, whereas all the operators were used in the evolution
of networks with threshold functions.

\begin{table*}[t]
  \centering
  \begin{tabular}{lp{7cm}} \hline
    \textbf{Operator} & \textbf{Description} \\ \hline
    \texttt{mutate\_weight} & A new value for the weight of an edge is chosen from the allowed values. \\
    \texttt{mutate\_connection} & An incoming connection to a node is given a different source. \\
    \texttt{mutate\_threshold} & A new value for the threshold of a node is chosen from the allowed values. \\
    \texttt{add\_edge} & A new incoming edge is added to the node. It connects to a random node and has a random
    weight. \\
    \texttt{delete\_edge} & One of the node's edges is chosen at random and removed. \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Mutation operators for the networks.}
  \label{mutations}
\end{table*}

\subsubsection{Evaluation}

Networks were evolved to perform specific boolean functions with four inputs and one output. Each network was therefore
run sixteen times, once for each of the possible input permutations. For each run, our nodes, which were designated as the input
nodes, had their activations set to the values of the given input permutation and their activations were clamped to
these values for the duration of the run. The network was then simulated for the number of timesteps specified in tables
\ref{threshold-parameters} and \ref{NAND-parameters}. At the end of the simulation, the value of designated output
output node was read and if it matched the output value of the target function, for the given input, the fitness of the
network was incremented by one. Networks could therefore have fitnesses in the range $[0, 16]$. 

\subsection{Evolutionary Algorithm}\label{sec:methods:ea}

The networks were evolved with an \textit{Evolutionary Algorithm} (EA) using deterministic tournament
selection \cite{EibenSmith2003}, applied 200 times per generation.
At each generation, two tournaments were created where the fittest genotype in the first tournament was cloned and replaced
the least fit in the second tournament.
Hence, each generation $\%10$ of the population was replaced.
Also, the EA used mutation only (there was no recombination operator).
Table \ref{evo-params} presents the EA parameters.  The following
subsections detail the EA setup for controller evolution using the binary and GRN encodings, respectively.

\begin{table}[t]
  \centering
  \begin{tabular}{lr}
  \hline
    \textbf{Parameter}                             & \textbf{Value}
    \\ \hline
    Population size                                & 10000 \\
    Births per generation                          & 10000 \\
    Tournament size                                & 100 \\
    Recombination                                  & None \\
    Generations per task variant switch            & 10 \\
    Number of generations                          & 500 \\
    Mutation                                       & See table \ref{mutations} \\
    Weight/connection mutation rate                & 0.05 \\
    Node mutation rate                             & 0.1 \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Evolutionary Algorithm Parameters}\label{evo-params}
\end{table}




\bibliographystyle{apalike} \bibliography{EvoComp}

\end{document}
