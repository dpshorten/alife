\documentclass[letterpaper]{article} \usepackage{natbib,alifeconf} \usepackage{hyperref}

\usepackage{placeins}
\renewcommand{\floatpagefraction}{.98}
\renewcommand{\topfraction}{.90}
\title{The Relationship Between the Evolvability and Robustness in the Evolution of Logic Networks}

%\author{David Shorten$^{1}$ \and Geoff Nitschke$^{1}$ \\ \mbox{}\\ $^1$University of
%  Cape Town \\ dshorten@cs.uct.ac.za}

\begin{document}
\maketitle

\begin{abstract}
Robustness and evolvability have traditionally been seen as conflicting properties of evolutionary systems, due to the
fact that selection requires heritable variation on which to operate.
Various recent studies have demonstrated that
organisms evolving in environments fluctuating non-randomly become better at adapting to these fluctuations,
that is, increase their \textit{evolvability}.
It has been suggested that this is due to the emergence of biases in the mutational neighborhoods of genotypes.
This paper examines a potential consequence of these observations,
that a large bias in certain areas of genotype space will lead to increased robustness in corresponding phenotypes.
The evolution of logic networks, which bear similarity to models of gene regulatory networks, is
simulated in environments which fluctuate between targets.
It was found that an increase in evolvability is concomitant with the emergence of highly robust genotypes.
Analysis of the genotype space elucidated that evolution finds regions containing robust genotypes coding
for one of the target phenotypes, where these regions overlap or are situated in close proximity.
Results indicate that genotype space topology impacts the relationship
between robustness and evolvability, where the separation of robust regions coding for the various targets was
detrimental to evolvability.
\end{abstract}

%\vspace{5mm}

\section{Introduction}

An open question in artificial and natural life is whether digital and natural organisms undergoing an evolutionary
process are able to become better at adapting to the selective pressures presented to them, that is to become more
\textit{evolvable} \citep{WagnerAltenberg1996}. However, this question is complicated by multiple definitions of
\textit{evolvability}. In both evolutionary biology \citep{pigliucci2008evolvability, pigliucci2010genotype,
  wagner2008robustness, parter2008facilitated} and \textit{Evolutionary Computation} (EC) \citep{taraporecomparing}, 
  for example, evolvability refers to either populations or individuals \citep{wilder2015reconciling}.  
  Similarly, within EC
\citep{EibenSmith2003} numerous definitions and associated metrics have been proposed.  For example, those that focus
exclusively on solution fitness \citep{grefenstette1999evolvability, reisinger2007acquiring} or variability of offspring
\citep{lehman2013evolvability}. \cite{taraporecomparing} developed a metric which incorporated both the fitness and
diversity of offspring. \cite{reisinger2005towards} measured evolvability as the ability of genotypes with various
representations to detect invariant patterns as commonalities in a changing fitness function.

Two significant definitions from the biological literature\footnote{For a review of evolvability in biology, the reader
  is referred to \cite{pigliucci2008evolvability}.}  which are pertinent to this work are those of
\textit{phenotypic accessibility} and \textit{adaptability}.
The former refers to the proportion of phenotypes that can
be accessed through evolution \citep{wagner2008robustness, CilibertiWagner2007, CilibertiWagnerB2007}.
This proportion
is determined by the topology of the genotype space (that is, which other genotypes a given genotype can directly mutate to) and
the G$\,\to\,$P mapping (which phenotype each genotype codes for).
Adaptability refers to the ability of organism to adapt to changes in the environment.
It has the advantage of not imposing an experimenter-biased measure on the system,
but merely asks whether evolution \textit{delivers the goods} \citep{budd2006origin, pigliucci2008evolvability}.
A prevailing hypothesis is that if the environment sufficiently varies over time, then organisms evolve the ability to be able to
evolve suitable adaptations to such environmental changes faster \citep{WagnerAltenberg1996, wagner2008robustness,
  draghi2008evolution}. \cite{crombach2008evolution} as well as \cite{draghi2008evolution} have demonstrated that
computational models of \textit{Gene Regulatory Networks} (GRNs) exhibit such \textit{evolvability}.

The representation problem in EC addresses the issue of how to represent and adapt (mutate and recombine) genotypes in
order that a broad range of complex solutions can be represented by relatively simple genotype encodings
\citep{wagner1996perspective}.
The choice of representation and associated operators has a significant impact on the
evolution of viable solutions and representations which facilitate evolution have been termed \textit{evolvable}
\citep{wagner1996perspective, rothlauf2006representations, simoes2014self}.
Similarly, in nature, genetic information
defining the form and function of an organism is stored within its genotype, however, the developmental process which
translates this information into phenotypes (the G$\,\to\,$P map) is not well understood \citep{pigliucci2010genotype}.
Yet, it has become clear that the G$\,\to\,$P map is neither one-to-one nor linear \citep{Gjuvsland2013}.
In many organisms and \textit{Ribonucleic acid} (RNA) folding
\citep{DavidDraper1992}, it has been found that many genotypes can code for a single phenotype and that genetic change
resulting from mutation is not proportional to phenotypic change \citep{pigliucci2010genotype, wagner2008robustness,
  parter2008facilitated}.

These features of the G$\,\to\,$P map have two important consequences on evolutionary dynamics.
The first is that they allow for the emergence of robust genotypes. As many genotypes can code for one phenotype, it is possible that some
number of a genotype's mutational neighbors code for the same phenotype as it does, thus affording the genotype a degree
of mutational robustness \citep{wagner2008robustness}.
The second consequence is that it becomes plausible that certain phenotypes are found in greater abundance in certain regions of the genotype space.
This in turn results in genotypic mutations having non-random effects on phenotypes, opening up the possibility that the distribution of these effects is
in some way advantageous \citep{hogeweg2012toward, watson2014evolution, parter2008facilitated, meyers2005evolution,
  gerhart2007theory, pavlicev2010evolution}.

We hypothesize that both robustness and mutational bias facilitate evolvability, however, the complex
relationship between robustness and evolvability makes this non-trivial to elucidate. 
Recent work has indicated that a certain degree of
robustness has a large benefit on evolvability interpreted either as phenotypic accessibility \citep{wagner2008robustness,
  CilibertiWagner2007} or adaptation to a goal \citep{draghi2010mutational}. These studies are predicated on the
assumption that a certain proportion of genotypes are non-viable and will never produce offspring. This constrains the
portion of the genotype space that is accessible from a given genotype. 
That is, once robustness rises above a certain threshold, genotypes of a given phenotype get connected in large neutral networks which can be traversed in
order to access a large variety of phenotypes and this access to variation allows for faster adaptation towards
a stationary target.

Mutational biases, however, can increase the likelihood of a given phenotype occurring in the neighborhood of
a given genotype, thus facilitating adaptation towards it. Various recent studies have demonstrated that
organisms evolving in environments fluctuating non-randomly are able to become better at adapting to these fluctuations,
thus increasing their \textit{evolvability} \citep{crombach2008evolution, draghi2008evolution}. 
It has been suggested that this is due to the emergence of biases in the mutational neighborhoods of genotypes
\citep{hogeweg2012toward, watson2014evolution}.  

Hence, this study aims to examine a potential consequence of these
biases, which is that increasing the bias towards a small number of phenotypes 
within a region of the genotype space will increase the robustness of the genotypes within that region. 
Experiments were conducted using the simulated evolution of logic networks in a fluctuating environment.  These logic
networks were comprised of nodes containing either NAND logic gates or threshold functions. 
The networks composed of
threshold functions closely resemble models of gene regulatory networks used to demonstrate the emergence of
evolvability in varying environments \citep{crombach2008evolution, draghi2008evolution}.
The networks composed of NAND logic gates were similar to those used to demonstrate the emergence of 
modularity \citep{KashtanAlon2005} 
as well as task performance speedup \citep{KashtanNoorAlon2007} in fluctuating environments. 

Results indicate that an increase in evolvability is concomitant with the emergence of highly robust genotypes. 
Analysis of the genotype space elucidated that evolution finds
regions containing robust genotypes coding for one of the target phenotypes and that these regions are either
overlapping or situated in close proximity. It was further found that the greater the separation between robust regions,
the greater the drop in fitness during target changes. This implies that less separated robust regions allows for
greater evolvability to emerge.

\begin{figure*}[!ht]
  \begin{minipage}[t]{0.45\textwidth}
    \centering
    {\includegraphics[width=70mm]{vis-NAND-far}}
  \end{minipage}
  \begin{minipage}[t]{0.45\textwidth}
    \centering
    {\includegraphics[width=70mm]{vis-NAND-close}}
  \end{minipage}\\
  \begin{minipage}[t]{0.45\textwidth}
    \centering
    {\includegraphics[width=70mm]{vis-threshold-far}}
  \end{minipage}
  \begin{minipage}[t]{0.45\textwidth}
    \centering
    {\includegraphics[width=70mm]{vis-threshold-close}}
  \end{minipage}\\

  \caption{ Visualizations of genotype space in the region of the best genotype at the end of an evolutionary run. Red
    dots are for genotypes who code for target one and blue dots are for genotypes who code for target two. The
    positioning of the dots was determined using the t-SNE algorithm \citep{van2008visualizing} which aims to preserve
    the distance between the dots in the higher dimensional space. The left column contains visualizations based on
    target pair one and the right for target pair two. The top row contains visualizations for NAND networks and the
    bottom for threshold networks.}
  \label{fig:visualization}
\end{figure*}

\begin{figure*}[!h]
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{NAND-far-very-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{NAND-far-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{NAND-far-late}}
  \end{minipage}\\
  
  \begin{minipage}[t]{\textwidth}
    \centering
    \textbf{NAND nodes, target pair one.}
  \end{minipage}\\
  
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{NAND-close-very-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{NAND-close-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{NAND-close-late}}
  \end{minipage} \\

  \begin{minipage}[t]{\textwidth}
    \centering
    \textbf{NAND nodes, target pair two.}
  \end{minipage}\\

  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{threshold-far-very-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{threshold-far-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{threshold-far-late}}
  \end{minipage}\\

  \begin{minipage}[t]{\textwidth}
    \centering
    \textbf{Threshold nodes, target pair one.}
  \end{minipage}\\

  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{threshold-close-very-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{threshold-close-early}}
  \end{minipage}
  \begin{minipage}[t]{0.32\textwidth}
    \centering
    {\includegraphics[width=60mm]{threshold-close-late}}
  \end{minipage} \\

    \begin{minipage}[t]{\textwidth}
    \centering
    \textbf{Threshold nodes, target pair two.}
  \end{minipage}

  \caption{Plots of the population average and maximum fitness as well as the average robustness of maximum fitness
    phenotypes averaged over the 20 runs for each of the four setups. The left column contains plots from early
    generations (0 - 40), the middle from slightly later generations (110 - 150) and the right from generations near the
    end of the run (710 - 750).}\label{fig:results}
\end{figure*}

\section{Methods}
\label{sec:methods}

\subsection{Network Models}\label{sec:GRN}

Networks were formed of nodes which could hold an activation value of either zero or one. Each node had an activation
function which was either a threshold or a NAND function. 
The activations of other connecting nodes were used as the inputs
to these functions, the specification of which nodes' activations to use thus implied the connectivity of the
network. Updates were done synchronously. The threshold function used is specified in equation \ref{grn-update}.
\begin{equation}\label{grn-update} s_i(t + 1) = \left\{ \begin{array}{lr} 1 :& \sum_j w_{ij}s_j(t) > \theta_i\\
    0 :& \sum_j w_{ij}s_j(t) \leq \theta_i \end{array} \right.
\end{equation}
Where, $s_i(t)$ is the activation of the $i$th node at simulation iteration $t$, $w_{ij}$ is the connection
weight of the directed edge from the $i$th to the $j$th node, and $\theta_i$ is the threshold of the $i$th node.
If no such connection exists then $w_{ij} = 0$. Table \ref{NAND-parameters} presents the parameters of the NAND networks
and table \ref{threshold-parameters} presents the parameters of the threshold networks.

\begin{table}[t]
  \centering
  \begin{tabular}{lr} \hline
    \textbf{Parameter}                           &              \textbf{Value Range} \\
    \hline
    Weights ($w_{ij}$)                            &              $[-2, 2]$ \\
    Thresholds ($\theta_{ij}$)                    &              $[-3, 3]$ \\
    Number of nodes                              &              $20$    \\
    Incoming connections per node                &              $[1, 10]$ \\
    Simulation iterations (maximum $t$)          &              $6$ \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Parameters for the networks composed of nodes with threshold functions}\label{threshold-parameters}
\end{table}

\begin{table}[t]
  \centering
  \begin{tabular}{lr} \hline
    \textbf{Parameter}                           &              \textbf{Value Range} \\
    \hline
    Number of nodes                              &              $20$    \\
    Incoming connections per node                &              $2$ \\
    Simulation iterations (maximum $t$)          &              $6$ \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Parameters for the networks composed of nodes with threshold functions}\label{NAND-parameters}
\end{table}

\subsection{Mutation Operators.}

Table \ref{mutations} specifies the mutation operators used in the evolutionary algorithm applied to evolve the networks.
The \texttt{mutate\_weight} operators were applied to the GRN node connections, where for every connection, the operator
was applied with a given probability (table \ref{evo-params}). All other mutation operators acted on nodes' activation
and threshold functions with a given probability (table \ref{evo-params}). In the evolution of networks with NAND
functions only the \texttt{mutate\_connection} operator was used, whereas all the operators were used in the evolution
of networks with threshold functions.

\begin{table*}[t]
  \centering
  \begin{tabular}{lp{7cm}} \hline
    \textbf{Operator} & \textbf{Description} \\ \hline
    \texttt{mutate\_weight} & A new value for the weight of an edge is chosen from the allowed values. \\
    \texttt{mutate\_connection} & An incoming connection to a node is given a different source. \\
    \texttt{mutate\_threshold} & A new value for the threshold of a node is chosen from the allowed values. \\
    \texttt{add\_edge} & A new incoming edge is added to the node. It connects to a random node and has a random
    weight. \\
    \texttt{delete\_edge} & One of the node's edges is chosen at random and removed. \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Mutation operators for the networks.}
  \label{mutations}
\end{table*}

\subsection{Evaluation.}

Networks were evolved to perform specific boolean functions with four inputs and one output. Each network was therefore
run sixteen times, once for each of the possible input permutations. 
For each run, nodes, designated as the \textit{input nodes}, had their activations set to the values of the given input 
permutation and their activations were clamped to these values for the duration of the run. 
The network was then simulated for the number of time steps specified in tables \ref{threshold-parameters} 
and \ref{NAND-parameters}. 

At the end of the simulation, the value of a designated \textit{output node} was read and if it matched 
the output value of the target function, for the given input, the fitness of the
network was incremented by one. Networks could therefore have fitness values in the range $[0, 16]$.

\subsection{Evolutionary Algorithm}\label{sec:methods:ea}

The networks were evolved with an \textit{Evolutionary Algorithm} (EA) using tournament selection and elitist
survivor selection \citep{EibenSmith2003}. Also, the EA used mutation only (there was no recombination operator). At
each generation, 5000 tournaments of ten genotypes were created. The winner of each tournament went on to produce a
single child. The fittest 5000 of the 10000 genotypes composed of children and current generation's population went on
to form the subsequent generation's population.  Table \ref{evo-params} presents the EA parameters. The choices of
selection operators as well as algorithm parameters were made as preliminary experiments showed that they were conducive to
the emergence of evolvability. 

\begin{table}[t]
  \centering
  \begin{tabular}{lr}
  \hline
    \textbf{Parameter}                             & \textbf{Value}
    \\ \hline
    Population size                                & 5000 \\
    Births per generation                          & 5000 \\
    Tournament size                                & 10 \\
    Recombination                                  & None \\
    Generations per task variant switch            & 10 \\
    Number of generations                          & 500 \\
    Mutation                                       & See table \ref{mutations} \\
    NAND connection mutation rate                  & 0.05 \\
    Threshold connection mutation rate             & 0.005 \\
    Node mutation rate                             & 0.02 \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Evolutionary Algorithm Parameters}\label{evo-params}
\end{table}

\section{Experiments}\label{sec:experiments}

For each of the two network types, evolution was run twenty times for 750 generations for each of two different target
pairs. Thus, four different evolutionary setups were each run twenty times. During evolution the goal was switched
between the two targets of the pair every ten generations. The target pairs are specified in table \ref{targets}. 
Target one of pair one is the function (a XOR b) AND (c XOR d) and target two of pair one is
(a XOR b) OR (c XOR d). This was done to maintain consistency with previous work on evolving logic networks in
fluctuating environments \citep{KashtanAlon2005, KashtanNoorAlon2007}. 

Target one of pair two is the same as in pair
one, however the second target was chosen to differ randomly in two outputs, as can be seen in table \ref{targets}. This
choice was made so as to ascertain the effect of alternating between targets that are more similar. Moreover, these
various experiment parameters were chosen because preliminary testing showed that they were conducive to the emergence
of evolvability.

During each evolutionary run, at the end of each generation, if there was at least one genotype in the population with
maximum possible fitness, then a test on the average robustness of such genotypes was run. That is, if at least one
genotype had reached the current target, as specified in table \ref{targets}, then such a test was run. This test
consisted of randomly drawing 200 such genotypes from the population, allowing for a given genotype to be drawn multiple
times, and then creating 15 mutated copies of this genotype, using the same EA mutation operator and parameters as
specified in section 2. The average robustness of the maximum fitness genotypes was then computed as the
proportion of these mutated copies which were also of maximum fitness.



\section{Results and Discussion}

Figure \ref{fig:results} presents plots of the maximum fitness, average fitness and robustness of maximum fitness
genotypes averaged over the 20 runs for each of the four setups. In early generations (0 - 150) the average and maximum
fitness respond more slowly to changes in the target as compared to later generations (710 - 750). A further observation
is that the average robustness of the maximum fitness genotypes that are present in the population increases over
evolutionary time.

A useful statistic in this context is the correlation between the average robustness achieved after evolving towards one
goal for the allotted ten generations and how quickly the population is able to adapt back to that goal after it has
spent the next ten generations evolving towards the other goal. That is, we need to test the hypothesis that robustness
and evolvability are correlated.  In order to measure rate of adaptation, the average fitness two generations after the
change back to the original goal was recorded.

We measure average absolute fitness, rather than the rate of fitness change (relative fitness) as an indicator of a population's
rate of adaptation.  We elected to use absolute fitness, since measuring relative fitness would unfairly benefit
genotypes whose fitness dropped the most after a change in the task environment.  That is, given two populations, the
one with the highest fitness a given interval after a task change is concluded to be the most adaptive.  Also, this
interpretation holds in the case that the other population suffers a greater fitness decrease after the task change,
which might subsequently lead it to having a faster fitness increase.

Thus, for each of the four setups, the Pearson correlation \citep{freedman2007statistics} between the average robustness
of maximum fitness genotypes at the end of each period evolving towards the first target of the pair and the average
fitness of the population two generations into the subsequent period evolving back towards this target was
calculated. Results deriving from periods where no genotypes in the population were of maximum fitness at the end of a
period of evolution towards the first target were excluded from the correlation computation for the reason that the
robustness could not be calculated in these instances. It was found that in all four setups, there was a positive
correlation between the robustness achieved and evolvability ($p < 0.01$).

A further pertinent question was the structure of this robustness across genotype space. It is plausible that evolution
could have found an area with robust genotype of both targets interspersed. Alternatively it could be alternating
between separate areas of robustness and these areas could be adjacent or separated by a greater distance and connected
by a strong fitness gradient. In order to ascertain this structure, visualizations of the genotype space were
constructed using the t-SNE algorithm \citep{van2008visualizing}.



For each of the four setups, evolution was run for 210 generations, where this number was chosen so that the population
would be between robust regions (assuming their existence). At the end of these runs, mutated copies of the fittest
genotype in the population were created using the same EA mutation operator and parameters as specified in section
2, although all mutation probability parameters were increased by a factor of two. This was because
preliminary testing found that smaller mutation probabilities caused the generation of sufficient target phenotype to be
too computationally expensive. These copies were created until there were 300 distinct genotypes which coded for each of
the two target phenotypes. These genotypes were subsequently fed into the t-SNE algorithm which arranged them in
2-dimensional space so as to preserve distance in the higher dimensional space as far as possible. That is, genotypes
which are closer together in genotype space, formed clusters in the visualization.

The metric used in the t-SNE algorithm was the hamming distance between genotypes. This is because two networks which
differ only in the wiring of one connection should always be considered to be the same distance apart, regardless of the
numerical identification values assigned to the nodes. A similar argument can be made for weights and thresholds.

The resultant visualizations are displayed in figure \ref{fig:visualization}. When NAND networks are being used on
target pair one there is a visible separation between the two robust regions, however, there would appear to be
substantially less separation in the other runs.

In order to gain a more quantitative understanding of this separation, the silhouette score
\citep{rousseeuw1987silhouettes} for each data set was computed. The silhouette score is a measure of the efficacy of a
clustering algorithm. Scores are in the range $[-1 , 1]$ and a positive score indicates distinct clusters, a negative
score indicates data points being placed in the wrong cluster and a score of zero indicates overlapping clusters. Thus,
in this instance, the silhouette score measured the efficacy of the phenotype of each genotype acting as its clustering
label.



In order to facilitate statistical comparisons, this process was run twenty times. The results of these runs are
displayed in table \ref{silhouettes}. The score for the NAND networks on target pair one is larger than for the other
setups. Furthermore, this difference is statistically significant ($p < 0.01$, Mann-Whitney U test with Bonferroni
correction \cite{FlanneryTeukolsky1986}).

\begin{table}[t]
  \centering
  \begin{tabular}{lll}
  \hline
                         & \textbf{Target One}        & \textbf{Target Two} \\
  \hline
  \textbf{Pair One}    & 0000011001100000           & 0110111111110110 \\
  \textbf{Pair Two}    & 0000011001100000           & 0000111001100100 \\
  \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Target pairs used. The bit strings represent the desired outputs for each input permutation, where the
    permutations are ordered by their integer interpretation. Thus, for instance, the first bit of the string represents
  the desired output for the permutation 0000, the last bit for permutation 1111 and the fourth bit for permutation 0100.}\label{targets}
\end{table}

%% \begin{table}[t]
%%   \centering
%%   \begin{tabular}{ll}
%%   \hline
%%   \textbf{Run}                  & \textbf{Correlation}  \\
%%   \hline
%%   NAND, pair one                & 0.50 \\
%%   NAND, pair two                & 0.16 \\
%%   Threshold, pair one          & 0.16 \\
%%   Threshold, pair two          & 0.31 \\
%%   \hline
%%   \end{tabular}
%%   \vspace{5mm}
%%   \caption[]{Pearson correlations \citep{freedman2007statistics} between the average robustness of maximum fitness genotypes at the end of evolution
%%     towards target one and the population average fitness two generations into the subsequent evolution back to this
%%   target.}\label{correlations}
%% \end{table}

\begin{table}[t]
  \centering
  \begin{tabular}{lr}
  \hline
  \textbf{Setup}                  & \textbf{Silhouette score}  \\
  \hline
  NAND, pair one                &  0.27 (0.08) \\
  NAND, pair two                & 0.08 (0.05) \\
  Threshold, pair one          &  0.06 (0.03)\\
  Threshold, pair two          & 0.06 (0.05) \\
  \hline
  \end{tabular}
  \vspace{5mm}
  \caption[]{Silhouette scores for generated genotypes in the neighborhood of the best genotype at then end of multiple
    runs for each setup. Standard deviations are in parentheses.}\label{silhouettes}
\end{table}

\FloatBarrier

The results indicate that an increase in evolvability, interpreted as adaptability, is concomitant with an increase in
the robustness of genotypes coding for the target phenotypes. This can be observed in the plots of fitness and
robustness displayed in figure \ref{fig:results}, where it can be seen that rapid adaptation to new targets coincided
with increased robustness. The link between evolvability and robustness is further supported by the statistically
significant correlation which was found between evolvability and robustness.

These results also support the hypothesis that increased evolvability is driven by biases in regions of genotype space
\citep{hogeweg2012toward, watson2014evolution} and the inference that a strong bias towards certain genotypes will
increase the robustness of these genotypes. Furthermore, the idea that robustness will facilitate adaptability in this
way should be intuitively clear. Finding a genotype within a given region will be aided by an abundance of this
genotype. Thus, as it can be argued that evolvability implies robustness and robustness implies evolvability, the
position of this paper is that the two phenomena are linked and occur in tandem.

Visualizations of the genotype space, as shown in figure \ref{fig:visualization}, demonstrated that the nature of
these biases can be non-trivial. That is, evolution does not necessarily settle on a region which is uniformly biased
towards both of the target phenotypes. Although this case was observed, the corresponding case of adjacent regions, each
biased to one of the phenotypes was also seen. The implications of these visualizations were supported by the
significant difference in the silhouette scores between these cases.

Furthermore, the difference between these cases appeared to have a strong influence on
the evolutionary dynamics, with a greater non-uniformity of the bias corresponding with larger drops in population
fitness during target changes. Moreover, the observation of these differences opens up interesting avenues of future
research. Pertinent questions are the causes of these structures, how large the distance between separated regions can
be and whether analogous structures emerge when evolution fluctuates between larger numbers of targets.

A further potential direction of future investigation is relating these results to work demonstrating the improvements
in adaption by robustness increasing the accessibility of the genotype space \citep{draghi2010mutational}. One can
predict that, under the assumption that a certain proportion of genotypes code for non-viable phenotypes, that an
increase in robustness biased towards viable phenotypes will have a larger impact on accessibility than a non-biased
increase in robustness. The increases in robustness observed here are strongly biased, and so should result in increased
access to the genotype space.

\section{Conclusion}

It was shown that in simulations of the evolution of logic networks in a fluctuating environment that an increase in
evolvability was concomitant with an increase in the robustness of genotypes coding for the target phenotypes. These
results support the hypothesis that evolvability is driven by biases in the genotype space, however, visualization of
the genotype space showed unexpected structure in the nature of these biases. It was found that instead of biases
towards different targets occurring in the same region, in some instances they are separated into adjacent
regions. These results contribute to a growing body of work \citep{wagner2008robustness, CilibertiWagner2007,
  draghi2010mutational} demonstrating the manner in which robustness can facilitate evolvability. However, it remains an
ongoing research project to elucidate the exact nature in which these results relate to one another.



\bibliographystyle{apalike} \bibliography{EvoComp}

\end{document}
